postpoint
=========

A simple HTTP server which accepts POST requests and returns some information about what it received to the caller.

Although this application's raison d'être is to serve as a bandwidth-testing endpoint for peergoggles[1], it could be useful in any case where a client needs to test upload capacity via HTTP.

The point is to be installable on the free tier of several IaaS / "cloud" app providers; instructions will eventually be provided to that end.

References
----------

1. https://github.com/gordon-morehouse/peergoggles
