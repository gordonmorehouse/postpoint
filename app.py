import os, time

from bottle import post, run, request, abort


current_milli_time = lambda: int(round(time.time() * 1000))


def get_file_size(f):
    oldpos = f.tell()
    f.seek(0, 2)
    length = f.tell()
    f.seek(oldpos)
    return length


@post('/cipollini/<post_time:re:[0-9]{13}>')
def handle_post(post_time):
    if not request.forms:
        abort(400, 'Bad request - please post a form.')
    
    response_dict = { }
    response_dict['your_time'] = int(post_time)
    response_dict['my_time'] = current_milli_time()

    if request.files and 'file' in request.files.keys():
        response_dict['your_file_size'] = get_file_size(request.files['file'].file)

    return response_dict

run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
